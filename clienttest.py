#!/usr/bin/python3
import socket
import sys

def main():
    if len(sys.argv) < 3:
        print('example input: clienttest.py 127.0.0.1 850 400')
        exit()
    server = sys.argv[1]
    cords = ' '.join(sys.argv[2:4])
    print(cords)
    count = 0
    while True:
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((server, 6969))
            s.sendall(bytes(cords, 'UTF-8'))
            sys.stdout.write("\033[K")
            print('connected')
            while True:
                number = input('number: ')
                s.sendall(bytes(number, 'UTF-8'))
                
        except KeyboardInterrupt:
            exit()
        except socket.error:
            sys.stdout.write("\033[K")
            print('failed to connect attempting to reconnect'+('.'*round(count)), end="\r")
            count=(count+0.0001)%3

if __name__ == '__main__':
    main()
