# BlueRev
--------------
* [Summary](#summary)
* [Pi Installation](#pi-installation)
* [Server Installation](#server-installation)

## Summary
BlueRev connects multiple raspberry pis to a server and 
sends what bluetooth devices are able to be seen by raspberry pis. 
The server checks to see which devices are being seen by what 
raspberry pi(s) and triangulates where the device may be based on
where the raspberry pis are. Coordinates are x pixels and y pixels
on the image called finalthing.png.

## Pi installation 
the 'piSetup.sh' script makes sure that dependencies are installed
for the 'bluePi.py' and writes it into the rc.local file so it is able 
to run on startup.

    wget https://raw.githubusercontent.com/ahyouknow/BlueRev/master/piSetup.sh
    sudo chmod +x piSetup.sh
    sudo ./piSetup.sh $SERVER_IP_ADDRESS $X_COORDINATE $Y_COORDINATE

## Server installation
There is not setup script for the server because all of the depencies should be
in the python standard library. Make sure that port 6969 is opened.

    wget https://raw.githubusercontent.com/ahyouknow/BlueRev/master/blueServer.py

The picture in the background is called finalthing.png it can be changed with a different picture it just has 
to be the same name. The picture is 1600x900 and the canvas is also 1600x900 so getting another picture it is 
recommended to be 1600x900.
if when you run the server script and it returns with an missing module error try to install it using pip
or the distro package manager. 

debian based distro example:

    sudo apt-get install python3-tk

